%token FOO BAR BAZ

%start<unit> foobar
%start<unit> foobaz

%%

foobar: FOO BAR { () }
foobaz: FOO BAZ { () }
