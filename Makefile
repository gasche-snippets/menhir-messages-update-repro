test.messages.new:
	menhir test.mly --list-errors > test.messages.new

test.messages.updated: test.messages
	menhir test.mly --update-errors test.messages > test.messages.updated

clean:
	rm -f test.messages{,.new,.updated}
